import Vue from "vue";
import VueRouter from "vue-router";
import Index from "../views/Index.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "index",
    component: Index,
  },
  {
    path: "/detailed/:tsid",
    name: "detailed",
    props: true,
    component: () => import("../views/Detailed.vue"),
    meta: {
      hideHeadFoot: true,
    },
  },
  {
    path: "/search",
    name: "search",
    component: () => import("../views/Search.vue"),
    meta: {
      hideHeadFoot: true,
    },
  },
  {
    path: "/reading_book",
    name: "reading_book",
    component: () => import("../views/Reading_Book.vue"),
    meta: {
      hideHeadFoot: true,
      Readbook: true,
    },
  },
  {
    path: "/opentime",
    name: "OpenTime",
    component: () => import("../views/OpenTime.vue"),
    meta: {
      hideHeadFoot: true,
    },
  },
  {
    path: "/open_two",
    name: "Open_two",
    component: () => import("../views/Open_Two.vue"),
    meta: {
      hideHeadFoot: true,
    },
  },
  {
    path: "/open_three",
    name: "Open_three",
    component: () => import("../views/Open_Three.vue"),
    meta: {
      hideHeadFoot: true,
    },
  },
  {
    path: "/open_fore",
    name: "Open_fore",
    component: () => import("../views/Open_Fore.vue"),
    meta: {
      hideHeadFoot: true,
    },
  },
  {
    path: "/resource",
    name: "Resource",
    component: () => import("../views/Resource.vue"),
  },
  {
    path: "/news",
    name: "News",
    component: () => import("../views/News.vue"),
  },
  {
    path: "/me",
    name: "Me",
    component: () => import("../views/Me.vue"),
  },
  {
    path: "/personal",
    name: "Personal",
    component: () => import("../views/Personal.vue"),
  },
  {
    path: "/resource_details/:bid",
    props: true,
    name: "ResourceDetails",
    component: () => import("../views/ResourceDetails.vue"),
    meta: {
      hideHeadFoot: true,
    },
  },
  {
    path: "/show_details",
    name: "ShowDetails",
    component: () => import("../views/Show_Details.vue"),
    meta: {
      hideHeadFoot: true,
    },
  },
  {
    path: "/cultural_details",
    name: "CulturalDetails",
    component: () => import("../views/CulturalDetails.vue"),
    meta: {
      hideHeadFoot: true,
    },
  },
  {
    path: "/me_collection",
    name: "我的收藏",
    component: () => import("../views/MeCollection.vue"),
    meta: {
      hideHeadFoot: true,
    },
  },
  {
    path: "/me_guide",
    name: "读者指南",
    component: () => import("../views/MeGuide.vue"),
    meta: {
      hideHeadFoot: true,
    },
  },
  {
    path: "/login",
    name: "登录",
    component: () => import("../views/Login.vue"),
    meta: {
      hideHeadFoot: true,
    },
  },
  {
    path: "/register",
    name: "注册",
    component: () => import("../views/register.vue"),
    meta: {
      hideHeadFoot: true,
    },
  },
  {
    path: "/poetryRead",
    name: "poetryRead",
    component: () => import("../views/poetryRead.vue"),
    meta: {
      hideHeadFoot: true,
    },
  },
  {
    path: "/notice",
    name: "最新公告",
    component: () => import("../views/NoTice.vue"),
    meta: {
      hideHeadFoot: true,
    },
  },
  {
    path: "/journalism",
    name: "国图新闻",
    component: () => import("../views/journalism.vue"),
    meta: {
      hideHeadFoot: true,
    },
  },
  {
    path: "/bulletin/:nid",
    props: true,
    name: "最新公告内容",
    component: () => import("../views/bulletin.vue"),
    meta: {
      hideHeadFoot: true,
    },
  },
  {
    path: "/attention",
    name: "请您关注",
    component: () => import("../views/attention.vue"),
    meta: {
      hideHeadFoot: true,
    },
  },
  {
    path: "/update",
    name: "Update",
    component: () => import("../views/Update.vue"),
    meta: {
      hideHeadFoot: true,
    },
  },
  {
    path: "/contact",
    name: "联系我们",
    component: () => import("../views/contact.vue"),
    meta: {
      hideHeadFoot: true,
    },
  },
  {
    path: "/updatepwd",
    name: "修改密码",
    component: () => import("../views/updatepwd.vue"),
    meta: {
      hideHeadFoot: true,
    },
  },
  {
    path: "/bookshelf",
    name: "我的书架",
    component: () => import("../views/bookshelf.vue"),
    meta: {
      hideHeadFoot: true,
    },
  },
];

const router = new VueRouter({
  // mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export default router;
