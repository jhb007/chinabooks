import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";

// 引入axios
import axios from "axios";
import VueAxios from "vue-axios";
// axios.defaults.baseURL = "http://172.234.15.58:2222/b1/";
axios.defaults.baseURL = "http://39.105.125.35:2222/b1/";
Vue.use(VueAxios, axios);

// 引入vant ui
import Vant from "vant";
import "vant/lib/index.css";
Vue.use(Vant);

//引入moment.js
import moment from "moment";
Vue.prototype.moment = moment;

// 全局加载 swiper的css文件
import VAS from "vue-awesome-swiper";
Vue.use(VAS);
import "swiper/css/swiper.css";

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount("#app");
