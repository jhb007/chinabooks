import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    user_id: window.sessionStorage.getItem("user_id"), //保存当前登录用户,
  },
  getters: {},
  mutations: {
    updateUser_id(state, value) {
      state.user_id = value;
      // 需要持久化存储，还需要将用户名存入sessionStorage
      window.sessionStorage.setItem("user_id", value);
    },
  },
  actions: {},
  modules: {},
});
